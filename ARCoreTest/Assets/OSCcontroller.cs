﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityOSC;
using GoogleARCore.Examples.HelloAR;

public class OSCcontroller : MonoBehaviour {

	private long lastTimeStamp;
	public GameObject ARC;
	private HelloARController arc;

	// Use this for initialization
	void Start () {
		OSCHandler.Instance.Init();
        lastTimeStamp = -1;
		ARC = GameObject.Find("Example Controller");
		arc = ARC.GetComponent<HelloARController>();
	}
	
	// Update is called once per frame
	void Update () {
		//  受信データの更新
        OSCHandler.Instance.UpdateLogs();
        //  受信データの解析
		string address;
        foreach (KeyValuePair<string, ServerLog> item in OSCHandler.Instance.Servers) {
            for (int i=0; i < item.Value.packets.Count; i++) {
                if (lastTimeStamp < item.Value.packets[i].TimeStamp) {
                    lastTimeStamp = item.Value.packets[i].TimeStamp;
                    //  アドレスパターン（文字列）
                    address = item.Value.packets[i].Address;
					if(address == "/scale"){
						//  引数
						var argx = (float)item.Value.packets[i].Data[0];
						var argy = (float)item.Value.packets[i].Data[1];
						var argz = (float)item.Value.packets[i].Data[2];
                    	//  処理
						var list = arc.andys;
						for(int j=0; j<arc.indexOfAndy; j++){
							list[j].transform.localScale = new Vector3(argx,argy,argz);
						}
					}
                    
                }
            }
        }
	}
}
