import c4d
#Welcome to the world of Python
import OSC

ip = 'xxx.xxx.xxx.xxx' #OSCサーバー(Android端末)のIPアドレス
port = 8001 #OSC通信用ポート番号

def genParameter(obj):
    if obj is None: return False
    #objにスライダーのユーザーデータを追加する
    baseCon = c4d.GetCustomDatatypeDefault(c4d.DTYPE_REAL) #浮動少数ユーザーデータの取得
    baseCon[c4d.DESC_NAME] = 'スケール' #ユーザデータの名前を変更
    baseCon[c4d.DESC_SHORT_NAME] = 'スケール'
    baseCon[c4d.DESC_ANIMATE] = c4d.DESC_ANIMATE_ON #アニメーション可能
    baseCon[c4d.DESC_UNIT] = c4d.DESC_UNIT_REAL #単位は実数値
    baseCon[c4d.DESC_CUSTOMGUI] = c4d.CUSTOMGUI_REALSLIDER #スライダーとする
    baseCon[c4d.DESC_MIN] = 0.01
    baseCon[c4d.DESC_MINSLIDER] = 0.01 #最小値(0.01倍)
    baseCon[c4d.DESC_MAX] = 2
    baseCon[c4d.DESC_MAXSLIDER] = 2 #最大値(2倍)
    baseCon[c4d.DESC_DEFAULT] = 1 #デフォルト値(1倍)
    baseCon[c4d.DESC_STEP] = 0.01 #スライダーの移動間隔
    u_data = obj.AddUserData(baseCon) #objにユーザデータを追加
    obj[u_data] = 1
    c4d.EventAdd()
    return u_data

def sendMsg(address, val):
    #valがリストのとき、中身を文字列にしてすべて送信
    if isinstance(val, list):
        #OSC通信の準備
        client = OSC.OSCClient()
        msg = OSC.OSCMessage()
        msg.setAddress(address)
        for i in range(len(val)):
            msg.append(val[i])

        #送信
        client.sendto(msg, (ip, port))
        print "sent to  %s ,[%s]" % (ip, msg)
        return

def main():
    null_obj = op.GetObject()
    uData = null_obj.GetUserDataContainer()
    #ヌル オブジェクトにユーザーデータがまだない場合のみ追加する
    if not uData:
        uData = genParameter(null_obj) #ヌル オブジェクトにスライダーを追加
        #null_obj.SetUserDataContainer(1, uData)

    val = null_obj[c4d.ID_USERDATA,1]
    sendMsg("/scale",[val,val,val])